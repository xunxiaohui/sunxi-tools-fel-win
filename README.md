# 使用说明

## 编译：直接执行build.bat

## 方法一

- 直接安装 driver 下的AllWinnerH3.inf
- 直接执行fel即可

## 方法二

- 先打开 libusb-win32-bin-1.2.6.0\bin 下的inf-wizard.exe
- 选中自己的板子对应的vid和pid的选项。next，还可以自己对应填写一个名称信息。例如AllWinnerH3。
- 他会自动生成一个inf文件和一些依赖文件。
- 安装这个inf文件，驱动安装完成。
- 直接执行fel即可

### 链接：https://gitee.com/xunxiaohui/sunxi-tools-fel-win